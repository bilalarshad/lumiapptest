import { StackNavigator } from "react-navigation";
import {
  Dashboard,
  Login,
  Register,
  ContactUs,
  Splash,
  BoardingScreens
} from "../views";

const Router = StackNavigator(
  {
    Login: {
      screen: Login
    },
    Dashboard: {
      screen: Dashboard
    },
    Register: {
      screen: Register
    },
    ContactUs: {
      screen: ContactUs
    },
    Splash: {
      screen: Splash
    },
    OnBoardingScreens: {
      screen: BoardingScreens
    }
  },
  {
    initialRouteName: "Splash",
    headerMode: "none"
  }
);

export default Router;
