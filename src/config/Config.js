export const Config = {
  ROOT_URL: "http://picdev.togethersupport.co.uk/webapi/mobile",
  REST_APIs: {
    Account: {
      Login: "Account/LoginMobile",
      ResetPassword: "Account/ChangePassword",
      LogOff: "Account/Logoff"
    }
  }
};
