import React, { Component } from "react";
import { View, TouchableOpacity, Platform, StatusBar } from "react-native";
import { connect } from "react-redux";
import {
  LumiiInput,
  LumiiText,
  LogoWithText,
  LumiiButton
} from "../components/common";
import { signMeIn } from "../actions/Account";
import { colors, screenKeys } from "../utils";

const styles = {
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "#FFFFFF",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  submitButton: {
    marginTop: 50
  },
  submitButtonText: {
    color: colors.lagoonGradient
  }
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  componentDidMount() {
    const { navigator } = this.props;
    signMeIn({ Email: "asadpkcs@gmail.com", Password: "asadas" }, navigator);
  }
  onForgot() {
    alert("Will be done later");
  }
  setFieldValue(text) {
    this.setState({ email: text });
  }

  handlePassword(text) {
    this.setState({ password: text });
  }

  navigateToRegister() {
    const { navigation } = this.props;
    navigation.navigate(screenKeys.Register);
  }
  navigateToDashboard() {
    const { navigation } = this.props;
    navigation.navigate(screenKeys.OnBoardingScreens);
  }
  render() {
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <View style={{ flex: 0.3, marginTop: 20 }}>
          <LogoWithText text="Login" />
        </View>
        <View
          style={{
            flex: 0.5,
            justifyContent: "center"
          }}
        >
          <LumiiInput
            name="email"
            placeholder="Username"
            onChangeText={text => this.setFieldValue(text)}
            keyboardType="email-address"
            value={email}
          />
          <LumiiInput
            name="password"
            placeholder="Password"
            onChangeText={text => this.handlePassword(text)}
            secureTextEntry
            value={password}
          />

          <View>
            <LumiiButton
              style={styles.submitButton}
              buttonText="Login"
              onPress={() => this.navigateToDashboard()}
            />
          </View>
        </View>
        <View
          style={{
            flex: 0.2,

            justifyContent: "flex-end",
            paddingBottom: 30
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <LumiiText text="Not signed up yet? " />
              <TouchableOpacity onPress={() => this.navigateToRegister()}>
                <LumiiText
                  text="Register"
                  fontFamily="defaultFontBold"
                  color={colors.lagoonGradient}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ paddingTop: Platform.OS === "ios" ? 10 : 0 }}>
            <TouchableOpacity
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => alert("ok")}
            >
              <LumiiText
                text="Forgot password "
                fontFamily="defaultFontBold"
                color={colors.lagoonGradient}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ accountReducer }) => {
  const { name } = accountReducer;
  return { name };
};

export default connect(mapStateToProps, {
  signMeIn
})(Login);
