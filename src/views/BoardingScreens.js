import React, { Component } from "react";
import { View, StatusBar, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  OnBoardText,
  LumiiLabel,
  LumiiText,
  LumiiButton
} from "../components/common";
import { colors, onBoarding, screenKeys, deviceWidth } from "../utils";

import * as Actions from "../actions/Dashboard";

const { whiteColor, blackColor, lagoonGradient } = colors;

const style = {
  containerStyle: {
    backgroundColor: whiteColor,

    flex: 1,
    flexDirection: "column",
    padding: 20
  },
  activeDotStyle: {
    backgroundColor: lagoonGradient,
    borderColor: lagoonGradient,
    borderRadius: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    height: 20,
    width: 20
  },
  dotStyle: {
    backgroundColor: whiteColor,
    borderColor: lagoonGradient,
    borderRadius: 10,
    borderWidth: 2,
    marginHorizontal: 10,
    height: 20,
    width: 20
  },
  submitButton: {},
  submitButtonText: {
    color: lagoonGradient
  },
  swiperContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    width: deviceWidth * 0.9,
    flex: 0.1
  },
  getStartedContainer: {
    backgroundColor: whiteColor,

    flex: 1
  },
  onBoardingContainer: {
    flex: 1,
    alignItems: "flex-start"
  },
  contentWrapper: {
    flex: 0.5,
    flexDirection: "column"
  }
};

class BoardingScreens extends Component {
  onYes() {
    const { actions, swiperIndex, navigation } = this.props;
    if (swiperIndex !== 5) {
      actions.setOnBoardingIndex(swiperIndex + 1);
      navigation.navigate(screenKeys.OnBoardingScreens);
    } else {
      navigation.navigate(screenKeys.Dashboard);
    }
  }
  onNoThanks() {
    const { actions, swiperIndex, navigation } = this.props;
    if (swiperIndex !== 5) {
      actions.setOnBoardingIndex(swiperIndex + 1);
      navigation.navigate(screenKeys.OnBoardingScreens);
    } else {
      navigation.navigate(screenKeys.Dashboard);
    }
  }
  showSwiper() {
    const { actions } = this.props;
    actions.showSwiper(true);
  }
  render() {
    const startingText = "Lumii is here to help, let's get started";
    const boardingHeaderText = "Tell us how Lumii can help you";

    const { swiperIndex, isSwiper, name } = this.props;
    return (
      <View style={style.containerStyle}>
        <StatusBar hidden />
        {isSwiper ? (
          <View style={style.onBoardingContainer}>
            <View style={{ flex: 0.8 }}>
              <LumiiLabel
                text={boardingHeaderText}
                color={blackColor}
                fontSize={28}
              />
              <OnBoardText
                onBoardText={onBoarding[swiperIndex].value}
                onYes={() => this.onYes()}
                onNoThanks={() => this.onNoThanks()}
              />
            </View>
            <View style={style.swiperContainer}>
              {onBoarding.map((result, index) => (
                <View
                  style={
                    index === swiperIndex
                      ? style.activeDotStyle
                      : style.dotStyle
                  }
                />
              ))}
            </View>
          </View>
        ) : (
          <View style={style.getStartedContainer}>
            <View style={style.contentWrapper}>
              <LumiiLabel
                text={`Hi ${name}`}
                fontFamily="defaultFontBold"
                color={blackColor}
                fontSize={36}
              />
              <LumiiLabel
                text={startingText}
                color={blackColor}
                fontFamily="defaultFontBold"
                fontSize={36}
              />
            </View>
            <View style={{ flex: 0.5, justifyContent: "center" }}>
              <LumiiButton
                style={style.submitButton}
                buttonText="Get started"
                onPress={() => this.showSwiper()}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}
const mapStateToProps = ({ dashboardReducer, accountReducer }) => {
  const { swiperIndex, isSwiper } = dashboardReducer;
  const { name } = accountReducer;
  return { swiperIndex, isSwiper, name };
};

export default connect(mapStateToProps, mapDispatchToProps)(BoardingScreens);
