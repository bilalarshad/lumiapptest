import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { HeartGuide } from './';

const style = {
  parentCnt: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column',
    padding: 20,
  },
};

class Dashboard extends Component {
  render() {
    return (
      <View style={style.parentCnt}>
        <StatusBar hidden />
        <HeartGuide />
      </View>
    );
  }
}

export default connect(null, {})(Dashboard);
