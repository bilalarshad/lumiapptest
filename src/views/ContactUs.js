import React, { Component } from "react";
import { View, TouchableOpacity, StatusBar, ScrollView } from "react-native";
import { connect } from "react-redux";
import {
  LumiiLabel,
  LumiiButton,
  LogoWithText,
  Paragraph,
  LumiiText
} from "../components/common";
import { colors, deviceHeight } from "../utils";

const { blackColor } = colors;
const styles = {
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    flexDirection: "column",
    padding: 20
  },
  buttonViewStyle: {},
  textStyle: {
    fontFamily: "defaultFont",
    fontSize: 16,
    color: blackColor,
    marginTop: 10,
    lineHeight: 30,
    textAlign: "justify"
  },
  bottomView: {
    marginTop: 20
  },
  contentStyle: {
    flexDirection: "column",
    flex: 0.6
  },
  bottomBtnContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    marginTop: 10
  },
  btnMainContainer: {
    marginTop: 30,
    flex: 0.2
  },
  submitButton: {
    backgroundColor: colors.whiteColor,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    borderColor: colors.lagoonGradient,
    borderWidth: 1,
    marginTop: 50,
    height: 50
  },
  viewBgColor: {
    backgroundColor: colors.whiteColor
  }
};

class ContactUs extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      textStyle,
      buttonViewStyle,
      contentStyle,
      container,
      bottomBtnContainer,
      btnMainContainer,
      submitButton,
      viewBgColor
    } = styles;
    return (
      <ScrollView style={viewBgColor}>
        <View style={container}>
          <StatusBar hidden />
          <View style={{ flex: 0.3 }}>
            <LogoWithText text="Thank you!" />
          </View>
          <View style={contentStyle}>
            <Paragraph
              textStyle={textStyle}
              text="We understand that experiencing a cardiac event is frightening so we are here to support you throughout this difficult time."
            />
            <Paragraph
              textStyle={textStyle}
              text="As you are one of the first people to use Lumii, your feedback is extremely important to us. It will help us to improve our service for you."
            />
            <Paragraph
              textStyle={textStyle}
              text="Are you happy for one of our team to contact you for feedback?"
            />
          </View>

          <View style={buttonViewStyle}>
            <TouchableOpacity style={styles.submitButton}>
              <LumiiText
                fontSize={24}
                fontFamily="defaultFontBold"
                text="Yes"
                color={colors.lagoonGradient}
              />
            </TouchableOpacity>
          </View>
          <View style={bottomBtnContainer}>
            <TouchableOpacity>
              <LumiiText
                fontSize={16}
                fontFamily="defaultFontBold"
                text="No thanks"
                color={colors.lagoonGradient}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ accountReducer }) => {
  const { name } = accountReducer;
  return { name };
};

export default connect(mapStateToProps, null)(ContactUs);
