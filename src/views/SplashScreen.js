import React, { Component } from "react";
import { View, Image, StatusBar } from "react-native";
import { connect } from "react-redux";
import { resetNavigationStack } from "../utils/CommonFunctions";
import {
  deviceHeight,
  deviceWidth,
  screenKeys,
  colors
} from "../utils/Constants";

const { lagoonGradient } = colors;
const splashLogo = require("../media/images/splash.png");

const styles = {
  imageStyle: {
    height: deviceHeight,
    width: deviceWidth,
    flex: 1
  },
  viewStyle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: lagoonGradient,
    flexDirection: "column",
    flex: 1
  }
};

class Splash extends Component {
  componentDidMount() {
    const { navigation } = this.props;

    setTimeout(() => {
      resetNavigationStack(navigation, screenKeys.Login);
    }, 2000);
  }
  componentWillUnmount() {
    clearInterval(this.splashTimer);
  }

  render() {
    return (
      <View style={styles.viewStyle}>
        <StatusBar hidden />
        <View>
          <Image style={styles.imageStyle} source={splashLogo} />
        </View>
      </View>
    );
  }
}

export default connect(null, null)(Splash);
