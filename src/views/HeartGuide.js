import React, { Component } from "react";
import { View, TouchableOpacity, StatusBar } from "react-native";
import { connect } from "react-redux";
import { Heart, LumiiLabel, LumiiText } from "../components/common";
import { mood, colors } from "../utils/Constants";

const { lagoonGradient, whiteColor, DeepLagoon } = colors;
const style = {
  parentCnt: {
    flex: 1,
    backgroundColor: whiteColor,
    flexDirection: "column"
  },
  viewContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 10,
    paddingVertical: 10,
    paddingLeft: 0
  },
  rattingContainer: {
    textAlign: "center",
    flexDitection: "column",
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  }
};

class HeartGuide extends Component {
  render() {
    const name = "Frank";
    return (
      <View style={style.parentCnt}>
        <StatusBar hidden />
        <View>
          <LumiiText
            fontFamily="defaultFontBold"
            fontSize={24}
            text={`Hi ${name}`}
            color={colors.lagoonGradient}
          />
        </View>
        <LumiiLabel
          text="How are you feeling today?"
          fontWeight="200"
          fontSize={18}
        />

        <View style={style.viewContainer}>
          {mood.map(result => (
            <TouchableOpacity style={style.rattingContainer}>
              <Heart />
              <LumiiText
                text={result.value}
                color={lagoonGradient}
                fontWeight="200"
                fontSize={14}
                key={result.key}
              />
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  }
}

export default connect(null, {})(HeartGuide);
