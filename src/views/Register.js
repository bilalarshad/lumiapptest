import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import {
  LumiiInput,
  LumiiText,
  LogoWithText,
  LumiiButton
} from "../components/common";
import { signMeIn } from "../actions/Account";
import { colors, screenKeys } from "../utils";
import { validateEmail } from "../utils/CommonFunctions";

const styles = {
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "#FFFFFF",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  submitButton: {
    backgroundColor: colors.whiteColor,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    borderColor: colors.lagoonGradient,
    borderWidth: 1
  },
  submitButtonText: {
    color: colors.lagoonGradient
  },
  viewBgColor: {
    backgroundColor: colors.whiteColor
  }
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      confirmPassword: ""
    };
  }
  onForgot() {
    alert("ok");
  }
  onRegister() {
    const { navigation } = this.props;
    navigation.navigate(screenKeys.ContactUs);
  }
  setFieldValue(text) {
    this.setState({ email: text });
  }

  handlePassword(text) {
    this.setState({ password: text });
  }

  navigateToRegister() {
    const { navigation } = this.props;
    navigation.navigate(screenKeys.Login);
  }
  onNameBlur() {
    const { name } = this.state;
    alert(name);
  }
  onEmailBlur() {
    const { email } = this.state;
    let response = validateEmail(email);
    alert(response);
  }
  onPasswordBlur() {
    const { password } = this.state;
    alert(password);
  }
  onConfirmPasswordBlur() {
    const { confirmPassword } = this.state;
    alert(confirmPassword);
  }
  render() {
    const { name, email, password, confirmPassword } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <View style={{ flex: 0.3, marginTop: 0 }}>
          <LogoWithText text="Register" />
        </View>
        <View
          style={{
            flex: 0.5,
            justifyContent: "center",
            marginTop: Platform.OS === "ios" ? 50 : 30
          }}
        >
          <LumiiInput
            name="name"
            placeholder="Name"
            onChangeText={text => this.setState({ name: text })}
            value={name}
            onBlur={() => this.onNameBlur()}
          />
          <LumiiInput
            name="email"
            placeholder="Email address"
            onChangeText={text => this.setState({ email: text })}
            value={email}
            onBlur={() => this.onEmailBlur()}
          />
          <LumiiInput
            name="passsword"
            placeholder="Password"
            onChangeText={text => this.setState({ password: text })}
            secureTextEntry
            value={password}
            onBlur={() => this.onPasswordBlur()}
          />
          <LumiiInput
            name="confirmpasssword"
            placeholder="Confirm password"
            onChangeText={text => this.setState({ confirmpasssword: text })}
            secureTextEntry
            value={confirmPassword}
            onBlur={() => this.onConfirmPasswordBlur()}
          />

          <View style={{ marginTop: 15 }}>
            <LumiiButton
              buttonText="Register"
              onPress={() => this.onRegister()}
            />
          </View>
        </View>
        <View
          style={{
            flex: 0.2,

            justifyContent: "flex-end"
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: Platform.OS === "ios" ? 30 : 30
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <LumiiText text="Already registered? " />
              <TouchableOpacity onPress={() => this.navigateToRegister()}>
                <LumiiText
                  fontFamily="defaultFontBold"
                  text="Login"
                  color={colors.lagoonGradient}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ paddingTop: Platform.OS === "ios" ? 10 : 0 }}>
            <TouchableOpacity
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => alert("ok")}
            >
              <LumiiText
                fontFamily="defaultFontBold"
                text="Forgot password "
                color={colors.lagoonGradient}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ accountReducer }) => {
  const { name } = accountReducer;
  return { name };
};

export default connect(mapStateToProps, {
  signMeIn
})(Register);
