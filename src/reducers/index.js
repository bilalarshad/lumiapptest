import { combineReducers } from "redux";
import Account from "./Account";
import ActivityIndicator from "./ActivityIndicator";
import Dashboard from "./Dashboard";

export default combineReducers({
  activityIndicatorReducer: ActivityIndicator,
  accountReducer: Account,
  dashboardReducer: Dashboard
});
