

const INITIAL_STATE = {
  name: '',
  password: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'FORM_CHANGED':
      return { ...state, [action.payload.prop]: action.payload.value };
    default:
      return state;
  }
};
