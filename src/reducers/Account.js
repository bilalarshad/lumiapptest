import { ActionTypes } from "../utils/ActionTypes";

const { LOGIN_FORM_CHANGED } = ActionTypes;
const INITIAL_STATE = {
  name: "David"
};

export default (state = INITIAL_STATE, action) => {
  const { payload } = action;
  switch (action.type) {
    case LOGIN_FORM_CHANGED:
      return { ...state, name: payload };
    default:
      return state;
  }
};
