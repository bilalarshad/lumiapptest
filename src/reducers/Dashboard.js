import { ActionTypes } from "../utils/ActionTypes";

const { ON_BOARDING_INDEX, SHOW_SWIPER } = ActionTypes;
const INITIAL_STATE = {
  swiperIndex: 0,
  isSwiper: false
};

export default (state = INITIAL_STATE, action) => {
  const { payload } = action;
  switch (action.type) {
    case ON_BOARDING_INDEX:
      return { ...state, swiperIndex: payload };
    case SHOW_SWIPER:
      return { ...state, isSwiper: payload };
    default:
      return state;
  }
};
