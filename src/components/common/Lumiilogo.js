import React, { Component } from "react";
import { Image, View, Dimensions } from "react-native";
import { deviceWidth } from "../../utils";

const logo = require("../../media/images/logo.png");

const Lumiilogo = ({ imageUrl }) => (
  <View>
    <Image
      source={logo}
      style={{
        width: deviceWidth * 0.3,
        height: deviceWidth * 0.3,
        alignSelf: "center",
        resizeMode: "contain"
      }}
    />
  </View>
);

export default Lumiilogo;
