import React from "react";
import PropTypes from "prop-types";
import { Text } from "react-native";
import { colors } from "../../utils";

const { blackColor, errorBgColor, blue, lagoonColor } = colors;

const LumiiText = ({ text, color, fontWeight, fontSize, fontFamily }) => {
  return (
    <Text
      style={{
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        fontFamily: fontFamily
      }}
    >
      {text}
    </Text>
  );
};

LumiiText.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  fontSize: PropTypes.string
};

LumiiText.defaultProps = {
  text: null,
  color: blackColor,
  fontSize: 14,
  fontFamily: "defaultFont"
};

export default LumiiText;
