import React from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity } from "react-native";

import { colors } from "../../utils";

const LumiiTouchable = ({ text, onPress }) => {
  return (
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignItems: "center"
      }}
      onPress={onPress}
    >
      <Text text={text} />
    </TouchableOpacity>
  );
};

LumiiTouchable.propTypes = {
  text: PropTypes.string.isRequired
};

LumiiTouchable.defaultProps = {
  text: null
};

export default LumiiTouchable;
