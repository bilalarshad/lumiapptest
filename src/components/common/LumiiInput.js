import React from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import { Input, Item, Icon, Left, Label } from "native-base";
import { colors, deviceWidth } from "../../utils";

const {
  blackColor,
  errorBgColor,
  blue,
  lagoonColor,
  fieldBorderBottom
} = colors;
const T = PropTypes;

const styles = {
  inputStyleMultiline: {
    fontSize: 15
  },
  inputItemStyle: {
    borderColor: fieldBorderBottom,
    marginBottom: 10
  },
  inputFont: {
    fontSize: 15
  },
  inputStyle: {}
};

const LumiiInput = ({
  placeholder,
  multiline,
  numberOfLines,
  value,
  error,
  onChangeText,
  keyboardType,
  secureTextEntry,
  iconName,
  onBlur
}) => {
  const { inputStyle, inputItemStyle, inputFont } = styles;
  return (
    <View>
      <Item style={inputItemStyle} floatingLabel>
        <Label>{iconName}</Label>
        <Input
          multiline={multiline}
          numberOfLines={numberOfLines}
          style={{ color: blackColor, fontFamily: "defaultFont" }}
          placeholderTextColor={blackColor}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
          secureTextEntry={secureTextEntry}
          onBlur={onBlur}
        />
      </Item>
      {error ? <Text>{error}</Text> : null}
    </View>
  );
};

LumiiInput.propTypes = {
  placeholder: T.string.isRequired,
  multiline: T.bool,
  numberOfLines: T.number,
  value: T.string.isRequired,
  error: T.string,
  onChangeText: T.func.isRequired,
  keyboardType: T.string,
  secureTextEntry: T.bool
};

LumiiInput.defaultProps = {
  error: null,
  multiline: false,
  numberOfLines: 1,
  keyboardType: "default",
  secureTextEntry: false
};
export default LumiiInput;
