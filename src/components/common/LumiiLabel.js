import React from "react";
import PropTypes from "prop-types";
import { Text } from "react-native";

import { colors } from "../../utils";

const { blackColor } = colors;

const LumiiLabel = ({ text, color, fontFamily, fontSize }) => (
  <Text style={{ fontFamily, fontSize, color }}>{text}</Text>
);

LumiiLabel.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  fontFamily: PropTypes.string,
  fontSize: PropTypes.number
};

LumiiLabel.defaultProps = {
  text: null,
  color: blackColor,
  fontFamily: "defaultFont",
  fontSize: 22
};

export default LumiiLabel;
