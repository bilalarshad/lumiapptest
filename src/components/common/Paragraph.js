import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../../utils/Constants";

const { white } = colors;
const T = PropTypes;
const styles = {
  txtStyle: {
    fontFamily: "defaultFont",
    fontSize: 14,
    color: white,
    marginTop: 10,
    lineHeight: 30
  }
};

const Paragraph = ({ text, textStyle }) => {
  const { txtStyle } = styles;
  return <Text style={textStyle === "" ? txtStyle : textStyle}>{text}</Text>;
};

Paragraph.propTypes = {
  text: T.string.isRequired
};

export default Paragraph;
