import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { getHeight } from "../../utils/constants";

const T = PropTypes;

const Footer = ({ onPress, text, linkText }) => {
  return (
    <View>
      <Text>{text} </Text>
      <TouchableOpacity onPress={onPress}>
        <Text>{linkText} </Text>
      </TouchableOpacity>
    </View>
  );
};

Footer.propTypes = {
  onPress: T.func.isRequired,
  text: T.string,
  linkText: T.string.isRequired
};

export default Footer;
