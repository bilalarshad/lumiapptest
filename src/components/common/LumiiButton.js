import React from "react";
import { Text, TouchableOpacity } from "react-native";
import { colors, deviceHeight } from "../../utils/Constants";

const { lagoonGradient, whiteColor } = colors;
const LumiiButton = ({ onPress, buttonText, style, textStyleOveride }) => {
  const { textStyle, buttonStyle } = styles;
  return (
    <TouchableOpacity style={[buttonStyle, style]} onPress={onPress}>
      <Text style={[textStyle, textStyleOveride]}> {buttonText} </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: "center",
    color: lagoonGradient,
    fontSize: 20,
    fontFamily: "defaultFontBold",
    fontWeight: "bold"
  },
  buttonStyle: {
    borderWidth: 1,
    borderColor: lagoonGradient,
    backgroundColor: whiteColor,
    borderRadius: 5,
    justifyContent: "center",
    padding: 10,
    height: 60
  }
};
export default LumiiButton;
