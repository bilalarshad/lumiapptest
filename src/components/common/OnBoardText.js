import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { LumiiText } from "./";
import { colors, deviceWidth } from "../../utils/Constants";

const T = PropTypes;
const styles = {
  textContainer: {
    marginTop: 20
  },
  headingText: {
    fontFamily: "defaultFontBold",
    flexDirection: "row",
    width: deviceWidth * 0.9,
    justifyContent: "flex-start",
    fontSize: 32
  },
  centralContainer: {
    flex: 1,
    flexDirection: "column"
  },
  noButton: {
    backgroundColor: colors.whiteColor,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 0,
    borderColor: colors.lagoonGradient,
    borderWidth: 0
  },
  yesButton: {
    backgroundColor: colors.whiteColor,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    borderColor: colors.lagoonGradient,
    borderWidth: 1,
    marginTop: 50
  },
  bottomContainer: {
    marginTop: 40,
    flexDirection: "column",
    alignSelf: "center",
    width: deviceWidth * 0.9,
    justifyContent: "flex-end"
  }
};

const OnBoardText = ({ onBoardText, onYes, onNoThanks }) => (
  <View style={styles.centralContainer}>
    <View style={styles.textContainer}>
      <Text style={styles.headingText}>{onBoardText} </Text>
    </View>
    <View style={styles.bottomContainer}>
      <TouchableOpacity style={styles.yesButton} onPress={onYes}>
        <LumiiText
          fontFamily="defaultFontBold"
          fontSize={24}
          text="Yes"
          color={colors.lagoonGradient}
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.noButton} onPress={onNoThanks}>
        <LumiiText
          fontFamily="defaultFontBold"
          fontSize={18}
          text="No thanks"
          color={colors.lagoonGradient}
        />
      </TouchableOpacity>
    </View>
  </View>
);
OnBoardText.propTypes = {
  onBoardText: T.string.isRequired,
  onYes: T.func.isRequired,
  onNoThanks: T.func.isRequired
};
OnBoardText.defaultProps = {
  onBoardText: "",
  onYes: () => {},
  onNoThanks: () => {}
};
export default OnBoardText;
