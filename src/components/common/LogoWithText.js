import React, { Component } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";

import { Lumiilogo, LumiiLabel } from "./";
import { deviceHeight } from "../../utils";
const LogoWithText = ({ text }) => (
  <View style={{ flexDirection: "column", alignItems: "center", flex: 0.8 }}>
    <Lumiilogo />
    <LumiiLabel
      text={text}
      style={{ paddingTop: deviceHeight * 0.05, fontFamily: "defaultFontBold" }}
    />
  </View>
);

LogoWithText.propTypes = {
  text: PropTypes.string.isRequired
};

LogoWithText.defaultProps = {
  text: null
};

export default LogoWithText;
