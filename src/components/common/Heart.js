import React from 'react';
import { View, Image } from 'react-native';

const Heart = () => (
  <View>
    <Image source={require('../../media/images/mood_outline.png')} />
  </View>
);

export default Heart;
