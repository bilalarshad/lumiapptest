import { ActionTypes } from "../utils/ActionTypes";

const { ON_BOARDING_INDEX, SHOW_SWIPER } = ActionTypes;
export function setOnBoardingIndex(index) {
  return {
    type: ON_BOARDING_INDEX,
    payload: index
  };
}
export function showSwiper(status) {
  return {
    type: SHOW_SWIPER,
    payload: status
  };
}
