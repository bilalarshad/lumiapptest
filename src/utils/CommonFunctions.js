import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { NavigationActions } from 'react-navigation';
import reducers from '../reducers';

export const setSelectedDate = (value) => {
  const months = [
    'JAN',
    'FEB',
    'MAR',
    'APR',
    'MAY',
    'JUN',
    'JUL',
    'AUG',
    'SEPT',
    'OCT',
    'NOV',
    'DEC',
  ];
  const days = ['SUN', 'MON', 'TUES', 'WED', 'THURS', 'FRI', 'SAT'];

  const todayDate = value === 0 ? new Date() : value;
  const dayStr = todayDate.getDay();
  const dayNum = todayDate.getDate();
  const month = todayDate.getMonth();
  return `${days[dayStr]} ${dayNum} ${months[month]}`;
};
export const resetNavigationStack = (navigation, pageName, params) => {
  const actionToDispatch = NavigationActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({ routeName: pageName, params })],
  });
  navigation.dispatch(actionToDispatch);
};
export const validateEmail = (email) => {
  const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
  return emailPattern.test({ email });
};
export const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
