import { Dimensions } from 'react-native';

export const deviceWin = Dimensions.get('window');
export const deviceWidth = deviceWin.width;
export const deviceHeight = deviceWin.height;

// ScreenKeys
export const screenKeys = {
  Login: 'Login',
  ContactUs: 'ContactUs',
  Register: 'Register',
  Dashboard: 'Dashboard',
  OnBoardingScreens: 'OnBoardingScreens',
};
// Colors
export const colors = {
  blackColor: '#000000',
  whiteColor: '#FFFFFF',
  lagoonColor: '#2E2099',
  lagoonGradient: '#2e20ff',
  DeepLagoon: '#150F46',
  yellowColor: '#fedf11',
  black: '#212121',
  white: '#FFEBEE',
  lightGray: '#eeeeee',
  lightBlue: '#eff3f6',
  gray: '#A9A9A9',
  darkGray: '#1a1c2d',
  bodyBg: '#f7f9fa',
  errorColor: '#0000FF',
  yellow: '#FFF8BA',
  errorBgColor: '#fff8ba',
  modalBackDropColor: '#D3D3D3',
  orangeColor: '#ff3803',
  buttonBgColor: '#fedf11',
  boxBorderColor: '#f1f1f1',
  blue: '#2500ff',
  blackTransparent: 'rgba(0,0,0,0.7)',
  blurTransparent: 'rgba(0,0,0,0.1)',
  whiteTransparent: 'rgba(255,255,255,0.8)',
  blueTransparent: 'rgba(0,118,255,0.9)',
  whitishGrey: '#ccc',
  blakishGray: '#333',
  mediumGray: '#d3d3d3',
  androidHeaderColor: '#1A1C29',
  searchfieldBG: '#d9dadd',
  dateSliderBG: '#e2e2e2',
  offRed: '#F4713C',
  fieldBorderBottom:'#2e2099',
};
// Mood
export const mood = [
  { key: '0', value: 'Awful' },
  { key: '1', value: 'Bad' },
  { key: '2', value: 'Ok' },
  { key: '3', value: 'Good' },
  { key: '4', value: 'Great' },
];

// On Boarding
export const onBoarding = [
  { key: '0', value: 'I want to be more active' },
  { key: '1', value: 'I want help with my diet' },
  { key: '2', value: 'I want help to cut down on alcohol' },
  { key: '3', value: 'I want help to stop smoking' },
  { key: '4', value: 'I want help with my medication' },
  { key: '5', value: 'I want to track my mood' },
];
